<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'compilation_termine'=> "Compilation terminée",
	'compil_chm_desc' => "Indiquer ici la ligne de commande permettant de lancer le compilateur CHM ou Latex sur votre système.",
	'compil_chm_label'   => "HTML => CHM",
	'compil_latex_label'   => "TeX => PDF",
	'compiler'          => "Compiler",
	'configuration_compilation' => "Configuration de la compilation",
	'export_termine'    => "Export terminé",
	'exporter_secteur'  => "Exporter un secteur en CHM ou LaTeX",
	'format'            => "Format",
	'format_html'       => "HTML (pour génération de fichier CHM)",
	'format_latex'      => "LaTeX (pour génération de fichier PDF)",
	'generation_html'   => "Génération de la documentation au format HTML",
	'generation_tex'    => "Génération de la documentation au format LaTeX",
	'langue'            => "Langue de l'export",
	'option_export'     => "Options de génération de l'export",
	'options_compilation'=>"Options de compilation",
	'secteur'           => "Secteur à exporter",
	'telecharger_chm'   => "Lien de téléchargement du fichier d'aide au format CHM",
	'telecharger_html'  => "Lien de téléchargement du fichier ZIP contenant le secteur au format HTML pour compilation CHM",
	'telecharger_pdf'  => "Lien de téléchargement du fichier PDF",
	'telecharger_tex'   => "Lien de téléchargement du fichier ZIP contenant le secteur au format LaTeX",
	'titre_page_configurer' => "Configurer le plugin CHM & Latex",
	'valider'           => "Valider"
);
?>
